#!/usr/bin/python

import sys
import threading, time
import math

import tornado.ioloop
import tornado.web
import tornado.websocket

from tornado.options import define, options, parse_command_line

define("port", default=8888, help="run on the given port", type=int)

server = None
client_count = 0

class MainHandler(tornado.web.RequestHandler):
	@tornado.web.asynchronous
	def get(self):
		action = self.get_argument("a", default=None)

		try:
			if action == None:
				with open('index.html', 'r') as f:
					data = f.read()
					with open('ip', 'r') as f2:
						data = data.replace('{IP_ADDRESS}', f2.read().strip())
						
					self.write(data)
			elif action == 'r':
				name = "%d" % len(server.clients)
				server.add_client(name)
				self.write('connected|%s' % name)
			elif action == 'u':
				user = self.get_argument("n", default='')
				if user in server.clients:
					speed = self.get_argument("s", default=None)
					direction = self.get_argument("d", default=None)
					jump = self.get_argument("j", default='0')

					if speed != None:
						server.clients[user].speed = float(speed)
						
					if direction != None:
						server.clients[user].direction = float(direction)

					if jump == '1':
						server.clients[user].jump = True

					server.clients[user].update_received()
		except:
			print 'Exception :', sys.exc_info()
			
		self.finish()

class WebServer(object):
	def __init__(self):
		self.clients = {}
		self.on_client_connected = None

	def set_client_connected(self, callback):
		self.on_client_connected = callback

	def start(self):
		self.app = tornado.web.Application([(r'/', MainHandler)])
		self.app.listen(options.port)
		tornado.ioloop.IOLoop.instance().start()

	def add_client(self, name):
		try:
			self.clients[name] = Player(name)
			if self.on_client_connected != None:
				self.on_client_connected(self.clients[name])
		except:
			print 'Exception :', sys.exc_info()

	def remove_client(self, name):
		if name in self.clients:
			del self.clients[name]


class Player(object):
	def __init__(self, username):
		self.username = username
		self.speed = 0
		self.direction = 0
		self.pos = [0, 0, 0]
		self.vspeed = 0
		self.jump = False
		self.last_update = time.time()
		self.destroy = False
		self.planted = True

	def update_received(self):
		self.last_update = time.time()

	def update(self, dt):
		try:
			if time.time() - self.last_update > 60:
				self.speed = 200
			
			self.pos[0] += self.speed * dt * math.cos(self.direction * math.pi/180.0)
			self.pos[1] += self.speed * dt * math.sin(self.direction * math.pi/180.0)
			if self.jump:
				if self.vspeed == 0:
					self.vspeed = 50
					self.pos[2] = .01
				self.jump = False

			if self.pos[2] <= 0 and self.planted:
				self.vspeed = 0
				self.pos[2] = 0
			else:
				self.vspeed -= dt * 98;
				self.pos[2] += dt * self.vspeed
		except:
			print 'Exception :', sys.exc_info()
			

server = WebServer()

if __name__ == "__main__":
	server.start()
