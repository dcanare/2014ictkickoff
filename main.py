#!/usr/bin/python

import random, threading, time, math
from kivy.app import App

from kivy.clock import Clock
from kivy.uix.widget import Widget
from kivy.animation import Animation
from kivy.uix.scatterlayout import ScatterLayout

from twitter_tools import TwitterTools
from facebook_tools import FacebookTools
from socialmediaevent import *
from socialmediawidget import *

import logging
from kivy.logger import Logger
logging.root = Logger

import webserver


tweeters = [ 'make_ict', 'thelaborparty', 'upfrontict' ]
tweetTags = [ '#wichita', '#ictkickoff', '#ictmixer', '#ictkickoffmixer' ]
events = ['649915065046941']
bookers = [ 'MakeICT', 'thelaborparty' ]

fb_limit = 'since=26+days+ago'
twitter_limit = 48

events = []
#tweeters = []
tweetTags = [ '#ict' ]
bookers = []


class ICTKickOffApp(App):
	def __init__(self, **kwargs):
		super(ICTKickOffApp, self).__init__(**kwargs)
		self.twitter = TwitterTools(twitter_limit)
		self.facebook = FacebookTools(fb_limit)

		for user in tweeters:
			self.twitter.add_user(user)
			
		for word in tweetTags:
			self.twitter.add_search(word)

		for event in events:
			self.facebook.add_event(event)

		for fbooker in bookers:
			self.facebook.add_profile(fbooker)

		self.players = []
		self.running = True
		webserver.server.set_client_connected(self.on_new_player)
		Clock.schedule_interval(self.update, 1.0 / 30.0)

	def random_plop(self, scatter, target_scale=None):
		scatter.center = (random.randint(10, Window.width - 20), random.randint(10, Window.height - 20))
		if target_scale == None:
			target_scale = random.random() + scatter.scale_min
			
		scatter.rotation = random.random() * 360
		scatter.scale = 0.01

		last_animation = Animation(scale=target_scale, duration=0.25)
		last_animation.bind(on_complete=self.animation_complete)
		
		animation = Animation(scale=0.125, duration=.25) \
			+ Animation(scale=target_scale * 1.125, duration=.75) \
			+ Animation(scale=target_scale / 1.125, duration=.25) \
			+ Animation(scale=target_scale * 1.0125, duration=.25) \
			+ Animation(scale=target_scale / 1.0125, duration=.25) \
			+ last_animation
			
		animation.start(scatter)

	def animation_complete(self, animation, widget):
		try:
			widget.allow_updates()
		except:
			pass

	def place_widget(self, widget):
		scatter = Scatter(scale_min=0.25)
		scatter.size = widget.size

		self.random_plop(scatter)

		scatter.add_widget(widget)
		self.root.add_widget(scatter)

	def player_destroyed(self, animation, widget):
		self.root.remove_widget(widget)

	def build(self):
		self.root = KeepOnTopContainer()
		self.registration_scatter = ScatterLayout(scale_min=0.25, size=(425, 400), rotation=180, pos=(-100, -100), scale=0.25)
		form = RegistrationForm(size=(425, 400), size_hint=(0.95, 0.95), pos_hint={'x':0.025, 'y':0.025})
		self.registration_scatter.add_widget(form)
		self.root.add_widget(self.registration_scatter)

		form_button = Button(text='!', font_size=48, pos=(20, 20), size=(75, 75))
		form_button.pos = (20, 20)
		form_button.bind(on_press=self.toggle_form)
		self.root.keep_on_top(form_button)

		self.logo = ScatterLayout(size=(490,90), rotation=180, pos=(1150,900))
		self.logo.add_widget(AsyncImage(source='graphics/greenlightgo.png'))
		self.logo.do_rotation = False
		self.logo.do_scale = False
		self.logo.do_translation = False
		self.root.keep_on_top(self.logo)
		
		self.qr = [
			ScatterLayout(size=(250, 250), pos=(1005, 20)),
			ScatterLayout(size=(250, 250), pos=(20, 760))
		]
		for qr in self.qr:
			qr.add_widget(AsyncImage(source='graphics/ipqr.png'))
			qr.do_rotation = False
			qr.do_scale = False
			qr.do_translation = False
			self.root.keep_on_top(qr)
		
		self.threads = [
			threading.Thread(target=self.update_facebook),
			threading.Thread(target=self.update_twitter),
			threading.Thread(target=webserver.server.start)
		]
		for thread in self.threads:
			thread.daemon = True
			thread.start()

		return self.root

	def toggle_form(self, button):
		self.root.remove_widget(self.registration_scatter)
		self.root.add_widget(self.registration_scatter)
		if self.registration_scatter.x < -50:
			animation = Animation(pos=(130, 130), rotation=360, scale=1, duration=2)
		else:
			animation = Animation(pos=(-100, -100), rotation=180, scale=0.25, duration=1)
		animation.start(self.registration_scatter)

	def update_facebook(self):
		while self.running:
			print "Updating facebook"
			for post in self.facebook.update():
				self.place_widget(FacebookWidget(post))
				time.sleep(4)
			time.sleep(30)

	def update_twitter(self):
		while self.running:
			print "Updating twitter"
			for tweet in self.twitter.update():
				self.place_widget(TwitterWidget(tweet))
				time.sleep(4)
			time.sleep(30)

	def on_new_player(self, player):
		player_widget = PlayerWidget(player, self, angle=180)
		
		self.players.append(player_widget)
		self.random_plop(player_widget, 1)
		self.root.keep_on_top(player_widget)
		
		player.pos[0] = player_widget.x
		player.pos[1] = player_widget.y
		player.direction = player_widget.rotation

	def update(self, dt):
		for p in self.players:
			if p.player.vspeed <= 0:
				p.player.planted = False
				for z in self.root.children:
					if z != p:
						if z.collide_widget(p):
							p.player.planted = True
							break
			p.update(dt)

	def remove_player_widget(self, widget):
		self.root.remove_widget(widget)
		if widget in self.players:
			self.players.remove(widget)



class PlayerWidget(Scatter):
	def __init__(self, player, container, **kwargs):
		super(PlayerWidget, self).__init__(**kwargs)
		self.player = player
		self.do_rotation = False
		self.do_scale = False
		self.do_translation = False
		self.updates_allowed = False
		self.container = container
		#self.add_widget(AsyncImage(source='graphics/minion.png'))
		self.frame_index = -1
		base = AsyncImage(source='graphics/minion.png')
		self.frames = [
			base,
			AsyncImage(source='graphics/minion-walk1.png'),
			base,
			AsyncImage(source='graphics/minion-walk2.png')
		]
		self.add_widget(base)

	def destroy_complete(self, animation=None, widget=None):
		self.container.remove_player_widget(self)

	def allow_updates(self):
		self.updates_allowed = True
		
	def update(self, dt):
		if self.updates_allowed:
			self.player.update(dt)

			if self.player.speed == 0:
				new_index = 0
			else:
				new_index = int(math.floor(time.time()*2)) % len(self.frames)
			if new_index != self.frame_index:
				self.frame_index = new_index
				self.clear_widgets()
				self.add_widget(self.frames[self.frame_index])
				
			self.center = (self.player.pos[0], self.player.pos[1])
			self.rotation = int(self.player.direction)
			self.scale = 1 + self.player.pos[2]/15
			if self.player.pos[2] <= -15:
				self.player.pos[2] = -15
				self.destroy_complete()
			

class KeepOnTopContainer(Widget):
	def __init__(self, **kwargs):
		super(KeepOnTopContainer, self).__init__(**kwargs)
		self.top_widgets = []

	def keep_on_top(self, widget):
		self.top_widgets.append(widget)
		super(KeepOnTopContainer, self).add_widget(widget)
		
	def remove_widget(self, widget):
		if widget in self.top_widgets:
			self.top_widgets.remove(widget)
		super(KeepOnTopContainer, self).remove_widget(widget)
		
	def add_widget(self, widget):
		super(KeepOnTopContainer, self).add_widget(widget)

		for w in self.top_widgets:
			super(KeepOnTopContainer, self).remove_widget(w)
			super(KeepOnTopContainer, self).add_widget(w)


if __name__ == '__main__':
	random.seed()
	ICTKickOffApp().run()
