import sys, datetime
import tweepy
import threading
from socialmediaevent import *

class TwitterTools(object):
	def __init__(self, age_limit_in_hours):
		print 'Authenticating with Twitter...'
		auth = tweepy.OAuthHandler('RBVC3u5ZJ6nzTQXrgHAPHw', 'kLL7L6fodDdm0g180iJ0MMPKvVIk8u1BMXmC3p5cV5Y')
		auth.set_access_token('831647130-gvWqj1nUHDfvwga6I8ZLtS44MWLuXxZtASSKa4X8', 'qtSRPj7vVJuFxLBNwqCr7VtKU94QsiVWUWtqucE0M6zdM')
		self.api = tweepy.API(auth)

		self.age_limit_in_hours = age_limit_in_hours

		self.users = []
		self.searchWords = []

		self.tweets = {}

	def add_user(self, name):
		t = threading.Thread(target=self._add_user, args=(name,))
		t.start()

	def _add_user(self, name):
		print 'Retrieving user info for %s...' % name
		self.users.append(self.api.get_user(name))

	def add_search(self, search):
		self.searchWords.append(search)

	def add_tweets(self, tweets):
		newTweets = []
		for tweet in tweets:
			try:
				if tweet.created_at < datetime.datetime.now() - datetime.timedelta(hours=self.age_limit_in_hours - 6):
					continue
						
				if tweet.id_str not in self.tweets:
					attachment = None
					if 'media' in tweet.entities:
						for media in tweet.entities['media']:
							if media['type'] == 'photo':
								size = media['sizes']['large']
								attachment = Attachment(media['media_url'], size['w'], size['h'])
								break
								
					newTweets.append(
						SocialMediaEvent(
							'@%s' % tweet.user.screen_name,
							tweet.user.profile_image_url,
							tweet.created_at,
							tweet.text,
							attachment
						)
					)
					self.tweets[tweet.id_str] = tweet
			except:
				print 'Exception :', sys.exc_info()
				print 'Failed to load tweet :', tweet.id

		return newTweets

	def update(self):
		new_tweets = []
		for user in self.users:
			print 'Downloading tweets for %s...' % user.screen_name
			new_tweets.extend(self.add_tweets(user.timeline()))
			
			print 'Downloading mentions for %s...' % user.screen_name
			new_tweets.extend(self.add_tweets(self.api.search('@%s' % user.screen_name)))

		for tag in self.searchWords:
			print 'Downloading tweets for %s...' % tag
			new_tweets.extend(self.add_tweets(self.api.search(tag)))

		return new_tweets

