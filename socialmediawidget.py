import sys, datetime

from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.uix.scatter import Scatter
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.button import Button
from kivy.uix.screenmanager import *
from kivy.uix.image import *
from kivy.graphics import *
from kivy.core.window import Window

class SocialMediaWidget(Widget):
	def __init__(self, event, icon, **kwargs):
		super(SocialMediaWidget, self).__init__(**kwargs)

		offset = 0
		if event.attachment != None:
			scale = 280.0 / event.attachment.width
			offset = scale * event.attachment.height
		self.size = (300, 150 + offset)

		with self.canvas:
			Color(1, 1, 1)
			Rectangle(pos=(0, 0), size=(300, 175 + offset))
			
			Color(0.25, 0.25, 0.25)
			Rectangle(pos=(1, 1), size=(298, 173 + offset))
			
			Color(0.125, 0.125, 0.125)
			Rectangle(pos=(5, 5), size=(290, 140 + offset))

			Color(0, 0, 0)
			Rectangle(pos=(15, 115 + offset), size=(50, 50))

		if event.attachment != None:
			self.image = AsyncImage(allow_stretch=True)
			self.image.size = (280, offset)
			self.image.pos = (10, 10)
			self.image.source = event.attachment.url
			self.add_widget(self.image)

		self.user_image = AsyncImage(source=event.userpic)
		self.user_image.pos = (16, 116 + offset)
		self.user_image.size = (48, 48)
		self.add_widget(self.user_image)
		
		self.username = Label(text=event.username, text_size=(200, 25))
		self.username.pos = (120, 110 + offset)
		self.add_widget(self.username)

		if event.timestamp != None:
			timestamp = event.timestamp + datetime.timedelta(hours=-6)
			self.timestamp = Label(
				text="%4d-%02d-%02d %02d:%02d" % (timestamp.year, timestamp.month, timestamp.day, timestamp.hour, timestamp.minute),
				text_size=(200, 25)
			)
			self.timestamp.pos = (120, 90  + offset)
			self.add_widget(self.timestamp)

		if event.text != None:
			self.text = Label(text=event.text, text_size=(270, None))
			self.text.pos = (100, 15 + offset)
			self.add_widget(self.text)

		self.icon = AsyncImage(source=icon)
		self.icon.pos = (274, 2)
		self.icon.size = (24, 24)
		self.add_widget(self.icon)

class TwitterWidget(SocialMediaWidget):
	def __init__(self, event, **kwargs):
		super(TwitterWidget, self).__init__(event, 'graphics/twitter.png', **kwargs)

class FacebookWidget(SocialMediaWidget):
	def __init__(self, event, **kwargs):
		super(FacebookWidget, self).__init__(event, 'graphics/fb.png', **kwargs)


class RegistrationForm(ScreenManager):
	def __init__(self, **kwargs):
		super(RegistrationForm, self).__init__(**kwargs)

		self.main_form = BoxLayout(orientation='vertical')
		with self.canvas:
			Color(.16, .16, .17)
			Rectangle(pos=(0, 0), size=self.size)

		self.name_box = TabTextInput(size_hint_y=.25)
		self.email_box = TabTextInput(size_hint_y=.25)
		self.comment_box = TabTextInput()
		self.infoBoxes = [
			ToggleButton(text='MakeICT'),
			ToggleButton(text='The Labor Party'),
			ToggleButton(text='Upfront Wichita'),
			ToggleButton(text='Code & Coffee'),
			ToggleButton(text='Startup Wichita')
		]
		
		self.main_form.add_widget(Label(text='Name :', size_hint_y=0.20))
		self.main_form.add_widget(self.name_box)
		self.main_form.add_widget(Label(text='Email :', size_hint_y=0.20))
		self.main_form.add_widget(self.email_box)
		self.main_form.add_widget(Label(text='Comments :', size_hint_y=0.20))
		self.main_form.add_widget(self.comment_box)

		self.main_form.add_widget(Widget(size_hint_y=0.075))
		self.main_form.add_widget(Label(text='Click the buttons below\nif you\'d like one of these groups to contact you!', size_hint_y=0.30, halign='center'))
		self.main_form.add_widget(Widget(size_hint_y=0.075))
		box_row = BoxLayout(size_hint_y=.2)
		box_row.add_widget(self.infoBoxes[0])
		box_row.add_widget(self.infoBoxes[1])
		self.main_form.add_widget(box_row)

		box_row = BoxLayout(size_hint_y=.2)
		box_row.add_widget(self.infoBoxes[2])
		box_row.add_widget(self.infoBoxes[3])
		box_row.add_widget(self.infoBoxes[4])
		self.main_form.add_widget(box_row)

		self.main_form.add_widget(Widget(size_hint_y=0.1))

		save_button = Button(text='Submit!', size_hint_y=.2)
		self.main_form.add_widget(save_button)
		save_button.bind(on_press=self.save_user)
		
		screen = Screen(name='main')
		screen.add_widget(self.main_form)
		self.add_widget(screen)

		self.feedback_button = Button(halign='center', font_size=24)
		self.feedback_button.bind(on_press=self.return_to_form)
		screen = Screen(name='feedback')
		screen.add_widget(self.feedback_button)
		self.add_widget(screen)

	def return_to_form(self, button):
		self.current = 'main'

	def save_user(self, button):
		popupText = 'Registration failed :('
		try:
			with open('registrations.txt', 'a') as f:
				f.write('%s\t' % self.name_box.text)
				f.write('%s\t' % self.email_box.text)
				f.write('%s\t' % self.comment_box.text)
				for b in self.infoBoxes:
					if b.state == 'down':
						f.write('%s|' % b.text)
						b.state = 'normal'
				f.write('\n')
				success = True
				self.name_box.text = ''
				self.email_box.text = ''
				self.comment_box.text = ''
				popupText = 'Registration saved! :)'
		except:
			print 'Exception :', sys.exc_info()
			print "Failed to save user!"
			print 'User: %s\t%s\t%s\t' % (self.name_box.text, self.email_box.text, self.comment_box.text),
			for b in self.infoBoxes:
				if b.state == 'down':
					print '%s|' % b.text,
			print ''

		self.feedback_button.text = '%s\n\n\nTouch here to return to\nthe registration form!' % popupText
		self.current = 'feedback'

class TabTextInput(TextInput):
	def _keyboard_on_key_down(self, window, keycode, text, modifiers):
		key, key_str = keycode
		if key == 9:
			children_count = len(self.parent.children)
			for i in range(children_count):
				if self.parent.children[i] == self:
					break
			next = i - 1
			while next != i:
				if next < 0:
					next = children_count - 1
				sibling = self.parent.children[next]
				if isinstance(sibling, TextInput):
					sibling.focus = True
					break
				next = next - 1
		else:
			super(TabTextInput, self)._keyboard_on_key_down(window, keycode, text, modifiers)
