import os, datetime, sys
from facepy import GraphAPI

from socialmediaevent import *

class FacebookTools(object):
	def __init__(self, limit=''):
		print 'Authenticating with Facebook...'
		# tokens from https://developers.facebook.com/tools/accesstoken/
		self.apis = [
			GraphAPI('CAAHJZAmNLVHwBACebJXhbZAtHuELMvhZA7az7QqTZBgYdgLiQJR62HX4ZAq7ZBUGOyZCe7BuiEPXOHacTQm3K0B2uRtltPCa4TbUDqCpmDFZCdXi9NPup8h3e4CHH4IaDENp5fCnNVN0KZAZCnZBHD0ymmJbh5kvZAZBSwk5quZBzAwo45tZCZBfZB1RCu0B4b0ixfg0JvwBiGgvInnOprgZDZD'),
			GraphAPI('502916566439036|9krnG486eMukYFhjKfHFujyq3vM'),
		]

		self.posts = {}
		self.events = []
		self.profiles = []
		self.limit = limit

	def add_event(self, eventID):
		self.events.append(eventID)

	def add_profile(self, profile):
		self.profiles.append(profile)

	def get_profile_pic(self, user_id):
		path = '/tmp/fb-%s.jpg' % user_id
		if not os.path.exists(path):
			try:
				f = open(path, 'wb')
				for api in self.apis:
					f.write(api.get('%s/picture?width=200&height=200' % user_id))
					break
			finally:
				f.close()
		return path

	def update(self):
		new_posts = []

		posts = []
		for event in self.events:
			print 'Loading event data for', event
			for api in self.apis:
				try:
					posts.extend(api.get('%s/feed?fields=picture,message,from,created_time,full_picture&%s' % (event, self.limit))['data'])
					break
				except:
					print 'Exception :', sys.exc_info()
					print 'FB Failed to retrieve event feed for %s' % event
			
		for profile in self.profiles:
			print 'Loading profile data for', profile
			for api in self.apis:
				try:
					posts.extend(api.get('%s/tagged?%s' % (profile, self.limit))['data'])
					break
				except:
					print 'Exception :', sys.exc_info()
					print 'FB Failed to retrieve tags for %s' % profile
					
			for api in self.apis:
				try:
					posts.extend(api.get('%s/statuses?%s' % (profile, self.limit))['data'])
					break
				except:
					print 'Exception :', sys.exc_info()
					print 'FB Failed to retrieve statuses for %s' % profile
		
		for post in posts:
			if post['id'] not in self.posts:
				try:
					print 'FB Loading post %s' % post['id']

					try:
						timestamp = datetime.datetime.strptime(post['created_time'], '%Y-%m-%dT%H:%M:%S+0000')
					except:
						timestamp = None

					try:
						message = post['message']
					except:
						message = None
						
					user = post['from']['name']
					profile_image_path = self.get_profile_pic(post['from']['id'])

					attachment = None
					if 'picture' in post:
						for api in self.apis:
							try:
								object_id = api.get('%s?fields=object_id' % post['id'])['object_id']
								photo = api.get('%s?fields=source,width,height' % object_id)
								attachment = Attachment('%s#.jpg' % photo['source'], photo['width'], photo['height'])
								break
							except:
								pass
								
						if attachment == None:
							if 'full_picture' in post:
								attachment = Attachment('%s#.jpg' % post['full_picture'], 100, 100)
							else:
								attachment = Attachment('%s#.jpg' % post['picture'], 100, 100)

					if message == None and attachment == None:
						raise Exception('No data to post!')
						
					self.posts[post['id']] = post
					new_posts.append(SocialMediaEvent(user, profile_image_path, timestamp, message, attachment))
				except:
					print 'Exception :', sys.exc_info()
					print 'Failed to load event data for ID:', post['id']


		return new_posts
