class Attachment(object):
	def __init__(self, url, width, height):
		self.url = url
		self.width = width
		self.height = height
	
class SocialMediaEvent(object):
	def __init__(self, username, userpic, timestamp, text, attachment=None):
		self.username = username
		self.userpic = userpic
		self.timestamp = timestamp
		self.text = text
		self.attachment = attachment

